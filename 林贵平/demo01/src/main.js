import { createApp } from 'vue'
import App from './App.vue'

createApp(App).mount('#app')
//关闭生产环境提示
// Vue.config.productionTip = false
//render 渲染 temldate
// new Vue({
//   //el:'#app',
//   // template:'<App></App>',
//   // components:{
//   //   App,
//   // }
//   render: h => h(App),
//   // components:{
//   //   App,
//   // },
//   // template:'<App></App>'
// }).$mount("#app")

// 原先的template 的元素，最终会不会被渲染到 html 中