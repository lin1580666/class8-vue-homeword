// 流程 引入 vue-roter， 写路由配置 path 跟 组件的对应关系,然后暴露 VueRouter 的实例
//引入vue-router,定义一些路由规则
import VueRouter from "vue-router";
//引用Page1页面

import productList from '../components/productList';

import discountList from '../components/discountList';
//单个路由均为对象类型，path代表的是路径,
// 就可以为url中的path，component代表组件

const routes=[
    {path:'/productList',component:productList},
    {path:'/discountList',component:discountList},

]

// 实例化VueRouter并将routes添加进去
export default new VueRouter({
//ES6简写，等于routes：routes
    routes
})