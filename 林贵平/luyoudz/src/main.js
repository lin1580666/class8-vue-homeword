import Vue from 'vue'
import App from './App.vue'

//引入vue-router,第三方模块不需要加路径
import VueRouter from 'vue-router';
//vue 怎么显示不同的页面-----------路由
Vue.use(VueRouter) //需要vue 去use 就是配置 VueRouter

//引用定义的router/index.js
import router from './router/index'

Vue.config.productionTip = false;

new Vue({
  el:'#app',
  //脚手架引入的vue是残缺的，为了性能
  render:createElement =>createElement(App),
    //一定要注入到vue的实例对象上
  router,

})



