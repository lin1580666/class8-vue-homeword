// 配置路由规则
import VueRouter from 'vue-router'

import OneBase from '../pages/OneBase'

import ListAa from '../pages/ListAa'
import TwoBase from '../pages/TwoBase'

import TwoListAa from '../pages/TwoListAa'
//引入组件
// import BodyBase from '../pages/BodyBase'
// // import BodyTwo from '../pages/BodyTwo'
// // import three from '../pages/three'
// import ListAa from '../pages/ListAa'

//创建并暴露一个路由器
export default new VueRouter({
   routes:[
       {
           path:'/OneBase',
           component: OneBase
       },

       {
           path:'/two/sanzhe',
           component:TwoBase
       },
       {
        name:'list',
        path: '/list/:id/:name',//path 配置这样，就能识别 key
        component: ListAa
    },
    {
        name:'Twolist',
        path: '/list/:id/:name',//path 配置这样，就能识别 key
        component: TwoListAa
    },
    ]
  
   
})

// export default router