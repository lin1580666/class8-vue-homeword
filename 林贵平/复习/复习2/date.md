**看抖音游戏禁止**

**模拟考（全部内容），周二上午**

**工信部证书**

200块

**复习内容Date 与 vue mvvm 原理**

时间相关的对象，

```
//创建
let date = new Date();//当前的时间对象

//创建对象时是否可以传递参数？可以

//可以是时间的字符串
let date2 = new Date('2022-5-20');

//还可以是时间戳
let timestamp = date.getTime();
let date3 = new Date(timestamp);

//可以填入年 月 日....
let date4=new Date(2022,4);

//获取时间戳
date.getTime();

//获取年份
date.getYear

.....


//setDate法根据本地时间来指定一个日期对象的天数，返回值，指定日期的当前的时分秒

//如果为 dayValue 指定0，那么日期就会被设置为上个月的最后一天
let date5 = new Date();

let returns=date5.setDate(19);

console.log(returns)

//setMonth 也是类似，自己试试.


```

**vue mvvm原理**

Object.defineProperty() 方法会直接在一个对象上定义一个新属性，或者修改一个对象的现有属性

```
let obj ={
    date:'520',
    action:'花1314',
    mood:'sad'
}


Object.defineProperty(obj,'action',{
    //添加get 与 
    // get 方法，获取这个属性时会触发
    get(){
        console.log('get 被出发胃');
        return obj.action;
    },
    //设置这个属性值会出发
    set(newVal){
       if(obj.action!=newVal){
           console.log('set 被触发了')
           obj.action =newVal;
       }
    }
})












